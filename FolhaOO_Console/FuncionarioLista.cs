﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_Console
{
    class FuncionarioLista
    {
       static List<Funcionario> funcionarios = new List<Funcionario>();

        public static Funcionario ObterFuncionario(Funcionario funcionario){
		    foreach(Funcionario x in funcionarios){
			    if(x.Cpf.Equals(funcionario.Cpf)){
				    return x;
			    }
		    }
		    return null;
	    }

        public static void AdicionarFuncionario(Funcionario funcionario)
        {
            funcionarios.Add(funcionario);
        }

        public static List<Funcionario> GetFuncionarios()
        {
            return funcionarios;
        }
    }
}

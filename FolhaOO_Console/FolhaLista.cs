﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_Console
{
    class FolhaLista
    {
        static private List<Folha> folhas = new List<Folha>();

        public static Folha ObterFolha(Folha folha){
		    foreach(Folha x in folhas){
			    if(x.Funcionario.Cpf.Equals(folha.Funcionario.Cpf) && x.Mes == folha.Mes && x.Ano == folha.Ano){
				    return x;
			    }
		    }
		    return null;
	    }

        public static void AdicionarFolha(Folha folha)
        {
            folhas.Add(folha);
        }

        public static List<Folha> GetFolhas()
        {
            return folhas;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
		    do{
			    Console.WriteLine("\n\n1 - Cadastrar funcionário");
			    Console.WriteLine("2 - Cadastrar folha");
			    Console.WriteLine("3 - Consultar folha");
			    Console.WriteLine("4 - Listar folha");
                Console.WriteLine("5 - Sair");
                Console.Write("Opção: ");
			    opc = int.Parse(Console.ReadLine());
			    switch(opc){
				    case 1:
					    cadastrarFuncionario();
					    break;
				    case 2:
					    cadastrarFolha();
					    break;
				    case 3:
					    consultarFolha();
					    break;
				    case 4:
					    listarFolha();
					    break;
			    }
		    }while(opc != 5);          
        }

        private static void cadastrarFuncionario(){
		    Funcionario funcionario = new Funcionario();
		    Console.Write("\n\n");
            Console.Write("Informe o CPF do funcionário: ");
		    funcionario.Cpf = Console.ReadLine();
		    if(FuncionarioNegocio.ValidarCpf(funcionario) == true){
			    if(FuncionarioLista.ObterFuncionario(funcionario) == null){
                    Console.Write("Informe o nome: ");
				    funcionario.Nome = Console.ReadLine();
				    FuncionarioLista.AdicionarFuncionario(funcionario);
			    }
			    else{
				    Console.WriteLine("\n\nFuncionário já cadastrado.");
			    }
		    }
		    else{
			    Console.WriteLine("\n\nCPF inválido.");
		    }
	    }

        private static void cadastrarFolha(){
		    Funcionario funcionario = new Funcionario();
		    Folha folha = new Folha();
		    Console.Write("\n\n");
            Console.Write("Informe o CPF do funcionário: ");
		    funcionario.Cpf = Console.ReadLine();
		    if(FuncionarioNegocio.ValidarCpf(funcionario) == true){
			    funcionario = FuncionarioLista.ObterFuncionario(funcionario);
			    if (funcionario != null){
				    Console.WriteLine("Funcionário: " + funcionario.Nome);
				    folha.Funcionario = funcionario;
                    Console.Write("Informe o mês: ");
				    folha.Mes = int.Parse(Console.ReadLine());
                    Console.Write("Informe o ano: ");
				    folha.Ano = int.Parse(Console.ReadLine());
				    if(FolhaLista.ObterFolha(folha) == null){
                        Console.Write("Informe as horas: ");
					    folha.Horas = int.Parse(Console.ReadLine());
                        Console.Write("Informe o valor da hora: ");
					    folha.Valor = float.Parse(Console.ReadLine());
					    FolhaLista.AdicionarFolha(folha);
				    }
				    else{
					    Console.WriteLine("\n\nEste cadastro já existe.");
				    }
			    }
			    else{
				    Console.WriteLine("Funcionário não cadastrado.");
			    }
		    }
		    else{
                Console.WriteLine("CPF inválido.");
		    }    		
	    }

        private static void consultarFolha(){
		    Funcionario funcionario = new Funcionario(); 
		    Folha folha = new Folha();
		    Console.Write("\n\n");
            Console.Write("Informe o CPF do funcionário: ");
		    funcionario.Cpf = Console.ReadLine();
		    if(FuncionarioNegocio.ValidarCpf(funcionario) == true){
			    folha.Funcionario = funcionario;
                Console.Write("Informe o mês: ");
			    folha.Mes = int.Parse(Console.ReadLine());
                Console.Write("Informe o ano: ");
			    folha.Ano = int.Parse(Console.ReadLine());
			    folha = FolhaLista.ObterFolha(folha);
			    if(folha != null){
				    Console.WriteLine("Funcionário: " + folha.Funcionario);
				    Console.WriteLine("Horas trabalhadas: " + folha.Horas);
				    Console.WriteLine("Valor da hora: " + folha.Valor.ToString("N2"));
				    float bruto = FolhaNegocio.CalcularSalarioBruto(folha);
				    Console.WriteLine("Salário bruto: " + bruto.ToString("N2"));
				    Console.WriteLine("INSS: " + FolhaNegocio.CalcularINSS(folha).ToString("N2"));
				    Console.WriteLine("IR: " + FolhaNegocio.CalcularIR(folha).ToString("N2"));
				    Console.WriteLine("FGTS: " + FolhaNegocio.CalcularFGTS(folha).ToString("N2"));
				    Console.WriteLine("Salário líquido: " + FolhaNegocio.CalcularSalarioLiquido(folha).ToString("N2"));
			    }
			    else{
				    Console.WriteLine("Dados não encontrados.");
			    }
		    }
        }

        private static void listarFolha(){
		    Console.Write("\n\n");
		    float total = 0;
            Console.Write("Informe o mês: ");
		    int mes = int.Parse(Console.ReadLine());
            Console.Write("informe o ano: ");
		    int ano = int.Parse(Console.ReadLine());
		    foreach(Folha x in FolhaLista.GetFolhas()){
			    if(x.Mes == mes && x.Ano == ano){
				    Console.WriteLine("Funcionário: " + x.Funcionario.Nome);
				    float bruto = FolhaNegocio.CalcularSalarioBruto(x);
				    Console.WriteLine("Salário líquido: " + FolhaNegocio.CalcularSalarioLiquido(x).ToString("N2"));
				    Console.WriteLine("-------------------------");
				    total += FolhaNegocio.CalcularSalarioLiquido(x);
			    }			
		    }
		    Console.WriteLine("Total de salários: " + total.ToString("N2"));
	    }   
    }
}

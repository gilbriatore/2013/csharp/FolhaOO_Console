﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_Console
{
    class Folha
    {
        public Funcionario Funcionario {set; get;}
        public int Mes {set; get;}
        public int Ano {set; get;}
        public int Horas {set; get;}
        public float Valor { set; get; }

        public Folha(){}
        public Folha(Funcionario Funcionario)
        {
            this.Funcionario = Funcionario;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_Console
{
    class Funcionario
    {
        public string Cpf { set; get; }
        public string Nome { set; get; }

        public Funcionario() { }
        public Funcionario(string Cpf, String Nome)
        {
            this.Cpf = Cpf;
            this.Nome = Nome;
        }
    }
}
